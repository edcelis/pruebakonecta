//Archivo fundamental para los eventos dentro del sitio
$(document).ready(function(){
  $(".jm-loadingpage").fadeOut("slow");
  
  $("#menu-toggle").click(function(e){
    e.preventDefault();
    $("#wrapper").toggleClass("menuDisplayed");
    if ($("#menu-toggle i").hasClass('fas fa-bars')) {
      $('#menu-toggle i').removeClass('fas fa-bars').addClass('fa fa-times');
    } else {
      $('#menu-toggle i').removeClass('fa fa-times').addClass('fas fa-bars');
    }
  });

  $("#actionProduct").click(function(){
    if ($("#nombre_producto").val() === '') {
      Swal.fire({
          icon: 'warning',
          title: '¡Información incompleta!',
          text: 'Por favor digite un nombre del producto'
      })
    } else if ($("#referencia_producto").val() === '') {
      Swal.fire({
          icon: 'warning',
          title: '¡Información incompleta!',
          text: 'Por favor digite la referencia del producto'
      })
    } else if ($("#categoria_producto").val() === '') {
      Swal.fire({
          icon: 'warning',
          title: '¡Información incompleta!',
          text: 'Por favor digite la categoria del producto'
      })
    } else if ($("#precio_producto").val() === '') {
      Swal.fire({
          icon: 'warning',
          title: '¡Información incompleta!',
          text: 'Por favor digite el precio del producto'
      })
    } else if ($("#peso_producto").val() === '') {
      Swal.fire({
          icon: 'warning',
          title: '¡Información incompleta!',
          text: 'Por favor digite el peso del producto'
      })
    } else if ($("#stock_producto").val() === '') {
      Swal.fire({
          icon: 'warning',
          title: '¡Información incompleta!',
          text: 'Por favor digite el stock del producto'
      })
    } else {
      Swal.fire({
        title: '¿Esta seguro de continuar?',
          text: "Al momento de continuar la información se almacera.",
          icon: 'question',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Si, estoy seguro!',
          cancelButtonText: 'No, cancelar',
      }).then((result) => {
        if (result.isConfirmed) {
          $("#formProduct").submit()
        } else {
          Swal.fire({
            icon: 'error',
            title: '¡Proceso cancelado!',
            text: 'Su proceso ha sido cancelado con exito'
          }) 
        }
      })  
    }
  })
  $("#actionVenta").click(function(){
    if ($("#concepto_venta").val() === '') {
      Swal.fire({
          icon: 'warning',
          title: '¡Información incompleta!',
          text: 'Por favor digite el concepto de la venta'
      })
    } else if ($("#producto").val() === '') {
      Swal.fire({
          icon: 'warning',
          title: '¡Información incompleta!',
          text: 'Por favor seleccione el producto de la venta'
      })
    } else if ($("#cantidad_venta").val() === '') {
      Swal.fire({
          icon: 'warning',
          title: '¡Información incompleta!',
          text: 'Por favor digite la cantidad de la venta'
      })
    } else {
      Swal.fire({
        title: '¿Esta seguro de continuar?',
          text: "Al momento de continuar la información se almacera.",
          icon: 'question',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Si, estoy seguro!',
          cancelButtonText: 'No, cancelar',
      }).then((result) => {
        if (result.isConfirmed) {
          $("#formVenta").submit()
        } else {
          Swal.fire({
            icon: 'error',
            title: '¡Proceso cancelado!',
            text: 'Su proceso ha sido cancelado con exito'
          }) 
        }
      })  
    }
  })
});
function toggle(id){
	$('.seccionToggle_'+id).slideToggle();
  if($('#'+id+' i').hasClass('fa fa-chevron-down')) {
    $('#'+id+' i').removeClass('fa fa-chevron-down').addClass('fa fa-chevron-up');
  } else {
    $('#'+id+' i').removeClass('fa fa-chevron-up').addClass('fa fa-chevron-down');
  }
}
