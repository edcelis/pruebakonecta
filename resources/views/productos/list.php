<?php 
error_reporting(0);
require_once("../../../include/Producto.php");
$ObjPro = new Producto();
$_info = $ObjPro->searchProducts();
?>
<!DOCTYPE html>
<html lang="es">
<head>
	<meta name="theme-color" content="#b4ca3d">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="">
	<link rel="shortcut icon" type="image/png" href="../../../resources/images/icon.png">
	<link rel="stylesheet" type="text/css" href="../../../assets/css/app.css">
	<link rel="stylesheet" type="text/css" href="../../../assets/css/home.css">
	<script type="text/javascript" src="../../../vendor/jquery/jquery.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
	<script type="text/javascript" src="../../../assets/js/app.js"></script>
	<script type="text/javascript" src="../../../assets/js/home.js"></script>
	<title>Konecta | Lista productos</title>
</head>
<body>
	<div class="jm-loadingpage"></div>
		<div id="wrapper">
	    	<!-- Sidebar -->
       	<div id="sidebar-wrapper">
      		<ul class="sidebar-nav">
      			<div class="text-center">
        			<img class="mb-4" src="../../../resources/images/logo.svg" alt="" width="160" height="40">
      			</div>
           		<li><a href="../../../index.php"><i class="fas fa-home"></i> Home</a></li>
           		<li><a href="new.php"><i class="far fa-bookmark"></i> Nuevo producto</a></li>
           		<li class="listActive"><a href="#"><i class="fas fa-clipboard-list"></i> Lista productos</a></li>
           		<li><a href="../ventas/new.php"><i class="fas fa-plus-circle"></i> Nueva venta</a></li>
           		<li><a href="../ventas/list.php"><i class="fas fa-clipboard-list"></i> Lista ventas</a></li>
      		</ul>
       	</div>
       	<!-- Page Content -->
       	<div id="page-content-wrapper">
         	<div class="container-fluid">
           	<div class="row">
           		<div class="col-lg-12 header">
                 <a id="menu-toggle"><i class="fas fa-bars"></i></a>
               </div>
           	</div>
             <section class="text-center sectionTop">
               <h3 class="subtitle"><strong><i class="fas fa-clipboard-list"></i> LISTADO PRODUCTOS</strong></h3>
             </section>
             <section class="row text-center">
              <article class="col-md-12">
                <div class="table-responsive">
                  <table class="table table-hover table-striped table-bordered">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>Nombre</th>
                        <th>Referencia</th>
                        <th>Precio</th>
                        <th>Stock</th>
                        <th>Acción</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php foreach ($_info as $key => $value) { ?>
                        <tr>
                          <td><?php echo $value['id_producto']; ?></td>
                          <td><?php echo $value['nombre_producto']; ?></td>
                          <td><?php echo $value['referencia_producto']; ?></td>
                          <td><?php echo $value['precio_producto']; ?></td>
                          <td><?php echo $value['stock_producto']; ?></td>
                          <td><a type="button" class="btn btn-primary" href="update.php?id=<?php echo $value['id_producto']; ?>"><i class="fas fa-edit"></i></a>
                            <a type="button" class="btn btn-danger" href="../../../include/actionProduct.php?id=<?php echo $value['id_producto']; ?>&accion=Borrar"><i class="fas fa-trash"></i></a>
                          </td>
                        </tr>
                      <?php } ?>
                    </tbody>
                  </table>
                </div>
              </article>
             </section>
         	</div>
       	</div>
      </div>
</body>
</html>