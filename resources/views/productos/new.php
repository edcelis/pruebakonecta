<!DOCTYPE html>
<html lang="es">
<head>
	<meta name="theme-color" content="#b4ca3d">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="">
	<link rel="shortcut icon" type="image/png" href="../../../resources/images/icon.png">
	<link rel="stylesheet" type="text/css" href="../../../assets/css/app.css">
	<link rel="stylesheet" type="text/css" href="../../../assets/css/home.css">
	<script type="text/javascript" src="../../../vendor/jquery/jquery.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
	<script type="text/javascript" src="../../../assets/js/app.js"></script>
	<script type="text/javascript" src="../../../assets/js/home.js"></script>
	<title>Konecta | Nuevo producto</title>
</head>
<body>
	<div class="jm-loadingpage"></div>
		<div id="wrapper">
	    	<!-- Sidebar -->
       	<div id="sidebar-wrapper">
      		<ul class="sidebar-nav">
      			<div class="text-center">
        			<img class="mb-4" src="../../../resources/images/logo.svg" alt="" width="160" height="40">
      			</div>
           		<li><a href="../../../index.php"><i class="fas fa-home"></i> Home</a></li>
           		<li class="listActive"><a href="#"><i class="far fa-bookmark"></i> Nuevo producto</a></li>
           		<li><a href="list.php"><i class="fas fa-clipboard-list"></i> Lista productos</a></li>
           		<li><a href="../ventas/new.php"><i class="fas fa-plus-circle"></i> Nueva venta</a></li>
           		<li><a href="../ventas/list.php"><i class="fas fa-clipboard-list"></i> Lista ventas</a></li>
      		</ul>
       	</div>
       	<!-- Page Content -->
       	<div id="page-content-wrapper">
         	<div class="container-fluid">
           	<div class="row">
           		<div class="col-lg-12 header">
                 <a id="menu-toggle"><i class="fas fa-bars"></i></a>
               </div>
           	</div>
             <section class="text-center sectionTop">
               <h3 class="subtitle"><strong><i class="fas fa-plus-circle"></i> NUEVO PRODUCTO</strong></h3>
             </section>
             <section>
              <form method="POST" action="../../../include/actionProduct.php" id="formProduct">
                <input type="hidden" name="accion" value="Crear">
                <article class="row">
                  <div class="col-md-4">
                    <div class="form-floating mb-3">
                      <input type="text" class="form-control" id="nombre_producto" name="nombre_producto" placeholder="Nombre producto">
                      <label for="nombre_producto">Nombre producto</label>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="form-floating mb-3">
                      <input type="text" class="form-control" id="referencia_producto" name="referencia_producto" placeholder="Referencia producto">
                      <label for="referencia_producto">Referencia producto</label>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="form-floating mb-3">
                      <input type="text" class="form-control" id="categoria_producto" name="categoria_producto" placeholder="categoria producto">
                      <label for="categoria_producto">Categoria producto</label>
                    </div>
                  </div>
                </article>
                <article class="row">
                  <div class="col-md-4">
                    <div class="form-floating mb-3">
                      <input type="number" class="form-control" id="precio_producto" name="precio_producto" placeholder="precio producto">
                      <label for="precio_producto">Precio producto</label>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="form-floating mb-3">
                      <input type="number" class="form-control" id="peso_producto" name="peso_producto" placeholder="peso producto">
                      <label for="peso_producto">Peso producto</label>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="form-floating mb-3">
                      <input type="number" class="form-control" id="stock_producto" name="stock_producto" placeholder="stock producto">
                      <label for="stock_producto">Stock producto</label>
                    </div>
                  </div>
                </article>
                <article class="row">
                  <div class="d-grid gap-2">
                    <button class="btn btn-lg btn-success btnGreen" type="button" id="actionProduct"><i class="far fa-paper-plane"></i> Guardar cambios</button>
                  </div>
                </article>
              </form>
             </section>
         	</div>
       	</div>
      </div>
</body>
</html>