<?php 
error_reporting(0);
require_once("../../../include/Producto.php");
$ObjPro = new Producto();
$_info = $ObjPro->searchProducts();
$_error = $_GET['error'];
?>
<!DOCTYPE html>
<html lang="es">
<head>
	<meta name="theme-color" content="#b4ca3d">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="">
	<link rel="shortcut icon" type="image/png" href="../../../resources/images/icon.png">
	<link rel="stylesheet" type="text/css" href="../../../assets/css/app.css">
	<link rel="stylesheet" type="text/css" href="../../../assets/css/home.css">
	<script type="text/javascript" src="../../../vendor/jquery/jquery.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
	<script type="text/javascript" src="../../../assets/js/app.js"></script>
	<script type="text/javascript" src="../../../assets/js/home.js"></script>
	<title>Konecta | Nueva venta</title>
</head>
<body>
	<div class="jm-loadingpage"></div>
		<div id="wrapper">
	    	<!-- Sidebar -->
       	<div id="sidebar-wrapper">
      		<ul class="sidebar-nav">
      			<div class="text-center">
        			<img class="mb-4" src="../../../resources/images/logo.svg" alt="" width="160" height="40">
      			</div>
           		<li><a href="../../../index.php"><i class="fas fa-home"></i> Home</a></li>
           		<li><a href="../productos/new.php"><i class="far fa-bookmark"></i> Nuevo producto</a></li>
           		<li><a href="../productos/list.php"><i class="fas fa-clipboard-list"></i> Lista productos</a></li>
           		<li class="listActive"><a href="#"><i class="fas fa-plus-circle"></i> Nueva venta</a></li>
           		<li><a href="list.php"><i class="fas fa-clipboard-list"></i> Lista ventas</a></li>
      		</ul>
       	</div>
       	<!-- Page Content -->
       	<div id="page-content-wrapper">
         	<div class="container-fluid">
           	<div class="row">
           		<div class="col-lg-12 header">
                 <a id="menu-toggle"><i class="fas fa-bars"></i></a>
               </div>
           	</div>
             <section class="text-center sectionTop">
               <h3 class="subtitle"><strong><i class="fas fa-plus-circle"></i> NUEVA VENTA</strong></h3>
             </section>
             <section>
              <form method="POST" action="../../../include/actionVenta.php" id="formVenta">
                <?php if ($_error != '') { ?>
                  <article class="row">
                    <div class="col-md-12">
                      <div class="alert alert-danger" role="alert">
                        La cantidad solicitada, supera el stock del producto en relación
                      </div>
                    </div>
                  </article>
                <?php } ?>
                <article class="row">
                  <div class="col-md-4">
                    <div class="form-floating mb-3">
                      <input type="text" class="form-control" id="concepto_venta" name="concepto_venta" placeholder="Nombre producto">
                      <label for="concepto_venta">Concepto</label>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="form-floating mb-3">
                      <select class="form-select" id="producto" name="producto" aria-label="producto">
                        <option selected value>Seleccione una opción...</option>
                        <?php foreach ($_info as $key => $value) { ?>
                          <option value="<?php echo $value['id_producto']; ?>"><?php echo $value['nombre_producto']; ?></option>
                        <?php } ?>
                      </select>
                      <label for="producto">Producto</label>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="form-floating mb-3">
                      <input type="number" class="form-control" id="cantidad_venta" name="cantidad_venta" placeholder="categoria producto">
                      <label for="cantidad_venta">Cantidad producto</label>
                    </div>
                  </div>
                </article>
                <article class="row">
                  <div class="d-grid gap-2">
                    <button class="btn btn-lg btn-success btnGreen" type="button" id="actionVenta"><i class="far fa-paper-plane"></i> Guardar cambios</button>
                  </div>
                </article>
              </form>
             </section>
         	</div>
       	</div>
      </div>
</body>
</html>