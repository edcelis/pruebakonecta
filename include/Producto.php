<?php
//Archivo para la ejecución de consultas
	require_once("Include.php");
	class Producto{
		function Producto(){}
		function searchProduct($_id){
			$ObjDB = new ConexionBD();
			$strSQL = "SELECT * FROM productos WHERE id_producto = $_id;";
			$arr = $ObjDB->db_result_to_array($strSQL);
			return $arr;
			$ObjDB = NULL;
		}
		function searchProducts(){
			$ObjDB = new ConexionBD();
			$strSQL = "SELECT * FROM productos;";
			$arr = $ObjDB->db_result_to_array($strSQL);
			return $arr;
			$ObjDB = NULL;
		}
		function searchVentas(){
			$ObjDB = new ConexionBD();
			$strSQL = "SELECT v.id_venta,v.concepto_venta, v.cantidad_venta, p.nombre_producto FROM ventas AS v INNER JOIN productos AS p ON v.id_producto = p.id_producto;";
			$arr = $ObjDB->db_result_to_array($strSQL);
			return $arr;
			$ObjDB = NULL;
		}
	}
?>