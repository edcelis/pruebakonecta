-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 20-01-2021 a las 04:40:06
-- Versión del servidor: 10.4.13-MariaDB
-- Versión de PHP: 7.2.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `example_konecta_bd`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos`
--

CREATE TABLE `productos` (
  `id_producto` int(11) NOT NULL,
  `nombre_producto` varchar(250) NOT NULL,
  `referencia_producto` varchar(250) NOT NULL,
  `precio_producto` int(11) NOT NULL,
  `peso_producto` varchar(100) NOT NULL,
  `categoria_producto` varchar(250) NOT NULL,
  `stock_producto` int(11) NOT NULL,
  `fecha_creacion_producto` date NOT NULL,
  `fecha_ultima_venta_producto` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `productos`
--

INSERT INTO `productos` (`id_producto`, `nombre_producto`, `referencia_producto`, `precio_producto`, `peso_producto`, `categoria_producto`, `stock_producto`, `fecha_creacion_producto`, `fecha_ultima_venta_producto`) VALUES
(1, 'Producto 1', '0-01', 1000, '1', 'Categoría 1', 98, '2021-01-19', '2021-01-20 03:01:30'),
(2, 'Producto 2', '0-02', 2000, '2', 'Categoría 2', 198, '2021-01-19', '2021-01-20 03:01:39'),
(3, 'Producto 3', '0-03', 3000, '3', 'Categoría 3', 298, '2021-01-19', '2021-01-20 03:01:51');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ventas`
--

CREATE TABLE `ventas` (
  `id_venta` int(11) NOT NULL,
  `concepto_venta` varchar(250) NOT NULL,
  `cantidad_venta` int(11) NOT NULL,
  `id_producto` int(11) NOT NULL,
  `fecha_registro_venta` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `ventas`
--

INSERT INTO `ventas` (`id_venta`, `concepto_venta`, `cantidad_venta`, `id_producto`, `fecha_registro_venta`) VALUES
(1, 'Venta 1', 2, 1, '2021-01-20 03:01:30'),
(2, 'Venta 2', 2, 2, '2021-01-20 03:01:30'),
(3, 'Venta 3', 2, 3, '2021-01-20 03:01:30');

--
-- Disparadores `ventas`
--
DELIMITER $$
CREATE TRIGGER `tr_update_stock` AFTER INSERT ON `ventas` FOR EACH ROW UPDATE productos
  SET stock_producto = stock_producto - new.cantidad_venta
  WHERE id_producto = new.id_producto
$$
DELIMITER ;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `productos`
--
ALTER TABLE `productos`
  ADD PRIMARY KEY (`id_producto`);

--
-- Indices de la tabla `ventas`
--
ALTER TABLE `ventas`
  ADD PRIMARY KEY (`id_venta`),
  ADD KEY `id_producto` (`id_producto`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `productos`
--
ALTER TABLE `productos`
  MODIFY `id_producto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `ventas`
--
ALTER TABLE `ventas`
  MODIFY `id_venta` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `ventas`
--
ALTER TABLE `ventas`
  ADD CONSTRAINT `ventas_ibfk_1` FOREIGN KEY (`id_producto`) REFERENCES `productos` (`id_producto`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
