# pruebaKonecta

Repositorio para almacenar la prueba técnica de la empresa grupo Konecta 

# Base de datos

La base de datos se encuentra en la carpeta ./db donde esta el sql

# Composer

El gestor de archivos se gestiona desde el archivo ./composer.JSON y la carpeta ./vendor

# Estilos y Eventos JS

Los estilos y eventos js se encuentran en la carpeta ./assets

# Imagenes y vistas

Las imagenes y vistas se encuentran dentro de la carpeta ./resources

# Conexiones, configuraciones y controladores

Se encuentran en la carpeta ./include

# Información basica

El actual sitio web esta diseñado con el objetivo de cumplir con la prueba tecnica para la empresa Grupo Konecta, se encuentra desarrollada en PHP puro, HTML5, CSS3, JavaScript, MySQL para la base de datos.   